package com.example.ocrapp.trash

import android.graphics.Rect
import androidx.camera.core.ImageAnalysis
import androidx.camera.core.ImageProxy
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.google.mlkit.vision.common.InputImage
import com.google.mlkit.vision.text.TextRecognition
import com.google.mlkit.vision.text.latin.TextRecognizerOptions


class LicencePlateAnalyzer(
//    private val licencePlateFormat: CameraPreviewConfig,
) : ImageAnalysis.Analyzer {

    private val detector = TextRecognition.getClient(TextRecognizerOptions.Builder().build())

    private var _resultText = MutableLiveData<List<String>>()
    val resultText: LiveData<List<String>> = _resultText

    private val setOfPlates = mutableSetOf<String>()

    @androidx.camera.core.ExperimentalGetImage
    override fun analyze(imageProxy: ImageProxy) {
        val mediaImage = imageProxy.image ?: return

        val rotationDegrees = imageProxy.imageInfo.rotationDegrees

        val imageHeight = mediaImage.height
        val imageWidth = mediaImage.width

        val convertImageToBitmap = ImageMediaUtils.convertYuv420888ImageToBitmap(mediaImage)

        val cropRect = Rect(0, 0, imageWidth, imageHeight)

//        val (widthCrop, heightCrop) = when (rotationDegrees) {
//            90, 270 -> Pair(
//                licencePlateFormat.heightCropPercent / 100f,
//                licencePlateFormat.widthCropPercent / 100f
//            )
//            else -> Pair(
//                licencePlateFormat.widthCropPercent / 100f,
//                licencePlateFormat.heightCropPercent / 100f
//            )
//        }

//        cropRect.inset(
//            (imageWidth * widthCrop / licencePlateFormat.widthCropRatio).toInt(),
//            (imageHeight * heightCrop / licencePlateFormat.heightCropRatio).toInt()
//        )

        val croppedBitmap =
            ImageMediaUtils.rotateAndCrop(convertImageToBitmap, rotationDegrees, cropRect)


        detector.process(InputImage.fromBitmap(croppedBitmap, 0))
            .addOnSuccessListener { textRaw ->

                val visionText = textRaw.textBlocks
                    .maxByOrNull { it.text.length }?.text
                    ?.trim()
                    ?.filter { it.isLetterOrDigit() }
                    .takeIf { it?.length in 3..10 }
                    ?.uppercase() ?: return@addOnSuccessListener


                if (setOfPlates.contains(visionText)) return@addOnSuccessListener

                if (setOfPlates.size >= 20)
                    setOfPlates.remove(setOfPlates.iterator().next()) /* remove first */
                setOfPlates.add(visionText)

                _resultText.postValue(setOfPlates.reversed())

            }
            .addOnCompleteListener {
                imageProxy.close()
            }
    }


}