//package com.example.ocrapp.trash
//
//import android.annotation.SuppressLint
//import android.app.Activity
//import android.content.ContentValues
//import android.content.Intent
//import android.graphics.Bitmap
//import android.media.Image
//import android.net.Uri
//import android.os.Build
//import android.os.Bundle
//import android.os.Environment
//import android.provider.MediaStore
//import android.util.Log
//import androidx.appcompat.app.AppCompatActivity
//import androidx.camera.core.*
//import androidx.camera.lifecycle.ProcessCameraProvider
//import androidx.core.content.ContextCompat
//import androidx.core.content.FileProvider
//import androidx.lifecycle.lifecycleScope
//import com.example.ocrapp.log
//import kotlinx.coroutines.Dispatchers
//import kotlinx.coroutines.launch
//import org.opencv.android.OpenCVLoader
//import org.opencv.android.Utils
//import org.opencv.core.*
//import org.opencv.imgproc.Imgproc
//import java.io.File
//import java.text.SimpleDateFormat
//import java.util.*
//import java.util.concurrent.ExecutorService
//import java.util.concurrent.Executors
//import kotlin.math.max
//import kotlin.math.pow
//import kotlin.math.sqrt
//
//class CameraActivity : AppCompatActivity() {
//    private var imageCapture: ImageCapture? = null
//
//    private lateinit var cameraExecutor: ExecutorService
//
//    override fun onCreate(savedInstanceState: Bundle?) {
//        super.onCreate(savedInstanceState)
//        viewBinding = ActivityCameraBinding.inflate(layoutInflater)
//        setContentView(viewBinding.root)
//
//        startCamera()
//
//        cameraExecutor = Executors.newSingleThreadExecutor()
//
//        viewBinding.imageCaptureButton.setOnClickListener {
//            takePhoto()
////            val uri = getPictureUri()
//
//        }
//
//        if (!OpenCVLoader.initDebug()) {
//            log("OpenCV trouble")
//        } else {
//            log("OpenCV is loaded")
//        }
//    }
//
//    private fun takePhoto() {
//        val imageCapture = imageCapture ?: return
//
//        val name = SimpleDateFormat(FILENAME_FORMAT, Locale.US)
//            .format(System.currentTimeMillis())
//        val contentValues = ContentValues().apply {
//            put(MediaStore.MediaColumns.DISPLAY_NAME, name)
//            put(MediaStore.MediaColumns.MIME_TYPE, "image/jpeg")
//            if (Build.VERSION.SDK_INT > Build.VERSION_CODES.P) {
//                put(MediaStore.Images.Media.RELATIVE_PATH, "Pictures/CameraX-Image")
//            }
//        }
//
//        val outputOptions = ImageCapture.OutputFileOptions
//            .Builder(
//                contentResolver,
//                MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
//                contentValues
//            )
//            .build()
//
//        imageCapture.takePicture(
//            outputOptions,
//            ContextCompat.getMainExecutor(this),
//            object : ImageCapture.OnImageSavedCallback {
//
//                override fun onError(exc: ImageCaptureException) {
//                    Log.d("qwerty", "Photo capture failed: ${exc.message}", exc)
//                }
//
//                override fun onImageSaved(output: ImageCapture.OutputFileResults) {
////                    val msg = "Photo capture succeeded: ${output.savedUri}"
////                    Toast.makeText(baseContext, msg, Toast.LENGTH_SHORT).show()
////                    Log.d("qwerty", msg)
//
//                    val intent = Intent()
//                    intent.putExtra("keyName", output.savedUri.toString())
//                    setResult(Activity.RESULT_OK, intent)
//                    finish()
//
//
//                }
//            }
//        )
//
//    }
//
//    private fun startCamera() {
//        val cameraProviderFuture = ProcessCameraProvider.getInstance(this)
//
//        cameraProviderFuture.addListener({
//            // Used to bind the lifecycle of cameras to the lifecycle owner
//            val cameraProvider: ProcessCameraProvider = cameraProviderFuture.get()
//
//            // Preview
//            val preview = Preview.Builder()
//                .setTargetResolution(android.util.Size(1920, 1080))
//                .build()
//                .also {
//                    it.setSurfaceProvider(viewBinding.viewFinder.surfaceProvider)
//                }
//
//            imageCapture = ImageCapture.Builder().build()
//
//            // Select back camera as a default
//            val cameraSelector = CameraSelector.DEFAULT_BACK_CAMERA
//
//            val imageAnalyzer = ImageAnalysis.Builder()
//                .setOutputImageRotationEnabled(true)
//                .setTargetResolution(android.util.Size(1920, 1080))
//                .setBackpressureStrategy(ImageAnalysis.STRATEGY_BLOCK_PRODUCER)
//                .build()
//                .also { analyzer ->
//                    analyzer.setAnalyzer(
//                        cameraExecutor,
//                        ImageAnalyzer(
//                            showProcessedImg = { showProcessedImage(it) },
//                            showSpentTime = { /*showSpentTimeOpenCV(it)*/ }
//                        )
//                    )
//                }
//
//            try {
//                // Unbind use cases before rebinding
//                cameraProvider.unbindAll()
//
//                // Bind use cases to camera
//                val camera = cameraProvider.bindToLifecycle(
//                    this, cameraSelector, preview, imageCapture, imageAnalyzer
//                )
//                if (camera.cameraInfo.hasFlashUnit()) {
//                    camera.cameraControl.enableTorch(false)
//                }
//            } catch (exc: Exception) {
//                Log.e("qwerty", "Use case binding failed", exc)
//            }
//
//        }, ContextCompat.getMainExecutor(this))
//    }
//
//    private fun showProcessedImage(bitmap: Bitmap) {
//        lifecycleScope.launch(Dispatchers.Main) {
////            viewBinding.proce.setImageBitmap(bitmap)
//            //jobOcr.let { job ->
//            //    if (job == null || !job.isActive) {
//            //        jobOcr = lifecycleScope.launch(Dispatchers.IO) {
//            //            ocrTesseract(bitmap)
//            //            //ocrMLKit(bitmap)
//            //            //delay(3000)
//            //        }
//            //    }
//            //}
//        }
//    }
//
//    private class ImageAnalyzer(
//        private val showProcessedImg: (bitmap: Bitmap) -> Unit,
//        private val showSpentTime: ((spentTime: Long) -> Unit)?
//    ) : ImageAnalysis.Analyzer {
//
//        @SuppressLint("UnsafeOptInUsageError")
//        override fun analyze(image: ImageProxy) {
//
//            image.image?.toRgba()?.let {
//                processImage(it)
//            }
//
//            image.close()
//        }
//
//        fun processImage(src: Mat) {
//            val startTime = System.currentTimeMillis()
//            val edges = Mat()
//
//            // load the image and compute the ratio of the old height
//            // to the new height, clone it, and resize it
//            val height = 600.0
//            val rat = src.height() / height
//            Imgproc.resize(src, edges, Size(src.width() / rat, height))
//
//            // convert the image to grayscale, blur it, and find edges in the image
//            Imgproc.cvtColor(edges, edges, Imgproc.COLOR_BGR2GRAY)
//            Imgproc.medianBlur(edges, edges, 5)
//
//            Imgproc.Canny(edges, edges, 75.0, 75.0 * 3)
//            Imgproc.dilate(edges, edges, Mat())
//
//            // ------> Start section SHOW RESULT: show the edge detected image
//            //Bitmap.createBitmap(edges.cols(), edges.rows(), Bitmap.Config.ARGB_8888).also {
//            //    Utils.matToBitmap(edges, it)
////                showProcessedImg(it)
//            //}
//            // ------> End section SHOW RESULT
//
//            val screenCnt: MutableList<MatOfPoint> = mutableListOf()
//            var contours: MutableList<MatOfPoint> = mutableListOf()
//            Imgproc.findContours(
//                edges, contours, Mat(), Imgproc.RETR_LIST, Imgproc.CHAIN_APPROX_SIMPLE
//            )
//            contours = contours
//                .sortedByDescending { cntr -> Imgproc.contourArea(cntr) }
//                .let { cntrs -> if (cntrs.size > 5) cntrs.take(5) else cntrs }
//                .toMutableList()
//
//            // loop over the contours
//            for (i in contours.indices) {
//                val cntr = MatOfPoint2f(*contours[i].toArray())
//                val approx2f = MatOfPoint2f()
//                // approximate the contour
//                val peri = Imgproc.arcLength(cntr, true)
//                Imgproc.approxPolyDP(cntr, approx2f, 0.02 * peri, true)
//                val approx = MatOfPoint(*approx2f.toArray())
//                // if our approximated contour has four points, then we
//                // can assume that we have found our screen
//                if (approx.toArray().size == 4 && Imgproc.isContourConvex(approx)) {
//                    screenCnt.add(approx)
//                }
//            }
//            // ------> Start section SHOW RESULT: show the contour (outline) of the piece of paper
//            if (screenCnt.size > 0) {
//                Imgproc.drawContours(src, screenCnt.map {
//                    MatOfPoint(*it.toArray().onEach { point ->
//                        point.x *= rat
//                        point.y *= rat
//                    })
//                }, -1, Scalar(0.0, 255.0, 0.0), 2)
//                Bitmap.createBitmap(src.cols(), src.rows(), Bitmap.Config.ARGB_8888)
//                    .also {
//                        Utils.matToBitmap(src, it)
//                        showProcessedImg(it)
//                    }
//            }
//
//            showSpentTime?.let { it(System.currentTimeMillis() - startTime) }
//        }
//
//        fun MatOfPoint.orderPoints(): MatOfPoint2f {
//            val pts = this.toArray()
//            // initialize a list of coordinates that will be ordered
//            // such that the first entry in the list is the top-left,
//            // the second entry is the top-right, the third is the
//            // bottom-right, and the fourth is the bottom-left
//            val rect = Array(4) { Point() }
//
//            // the top-left point will have the smallest sum, whereas
//            // the bottom-right point will have the largest sum
//            val s = pts.map { point -> point.x + point.y }
//            rect[0] = pts[s.indexOfFirst { it == s.min() }]
//            rect[2] = pts[s.indexOfFirst { it == s.max() }]
//
//            // now, compute the difference between the points, the
//            // top-right point will have the smallest difference,
//            // whereas the bottom-left will have the largest difference
//            val d = pts.map { point -> point.y - point.x }
//            rect[1] = pts[d.indexOfFirst { it == d.min() }]
//            rect[3] = pts[d.indexOfFirst { it == d.max() }]
//            //# return the ordered coordinates
//            return MatOfPoint2f(*rect)
//        }
//
//        fun Mat.fourPointTransform(pts: MatOfPoint): Mat {
//            // obtain a consistent order of the points and unpack them
//            // individually
//            val rect = pts.orderPoints()
//            val (tl, tr, br, bl) = rect.toArray()
//            // compute the width of the new image, which will be the
//            // maximum distance between bottom-right and bottom-left
//            // x-coordinates or the top-right and top-left x-coordinates
//            val widthA = sqrt(((br.x - bl.x).pow(2)) + ((br.y - bl.y).pow(2)))
//            val widthB = sqrt(((tr.x - tl.x).pow(2)) + ((tr.y - tl.y).pow(2)))
//            val maxWidth = max(widthA, widthB)
//            // compute the height of the new image, which will be the
//            // maximum distance between the top-right and bottom-right
//            // y-coordinates or the top-left and bottom-left y-coordinates
//            val heightA = sqrt(((tr.x - br.x).pow(2)) + ((tr.y - br.y).pow(2)))
//            val heightB = sqrt(((tl.x - bl.x).pow(2)) + ((tl.y - bl.y).pow(2)))
//            val maxHeight = max(heightA, heightB)
//            // now that we have the dimensions of the new image, construct
//            // the set of destination points to obtain a "birds eye view",
//            // (i.e. top-down view) of the image, again specifying points
//            // in the top-left, top-right, bottom-right, and bottom-left
//            // order
//            val dst = MatOfPoint2f(
//                Point(0.0, 0.0),
//                Point(maxWidth - 1, 0.0),
//                Point(maxWidth - 1, maxHeight - 1),
//                Point(0.0, maxHeight - 1)
//            )
//            // compute the perspective transform matrix and then apply it
//            val warped = Mat()
//            Imgproc.warpPerspective(
//                this, warped,
//                Imgproc.getPerspectiveTransform(rect, dst),
//                Size(maxWidth, maxHeight)
//            )
//            // return the warped image
//            return warped
//        }
//
//        fun Image.toRgba(): Mat {
//            val rgba = Mat()
//            val planes: Array<Image.Plane> = this.planes
//            val w: Int = this.width
//            val h: Int = this.height
//            val chromaPixelStride = planes[1].pixelStride
//            return if (chromaPixelStride == 2) { // Chroma channels are interleaved
//                assert(planes[0].pixelStride == 1)
//                assert(planes[2].pixelStride == 2)
//                val yPlane = planes[0].buffer
//                val yPlaneStep = planes[0].rowStride
//                val uvPlane1 = planes[1].buffer
//                val uvPlane1Step = planes[1].rowStride
//                val uvPlane2 = planes[2].buffer
//                val uvPlane2Step = planes[2].rowStride
//                val yMat = Mat(h, w, CvType.CV_8UC1, yPlane, yPlaneStep.toLong())
//                val uvMat1 = Mat(h / 2, w / 2, CvType.CV_8UC2, uvPlane1, uvPlane1Step.toLong())
//                val uvMat2 = Mat(h / 2, w / 2, CvType.CV_8UC2, uvPlane2, uvPlane2Step.toLong())
//                val addrDiff = uvMat2.dataAddr() - uvMat1.dataAddr()
//                if (addrDiff > 0) {
//                    assert(addrDiff == 1L)
//                    Imgproc.cvtColorTwoPlane(yMat, uvMat1, rgba, Imgproc.COLOR_YUV2RGBA_NV12)
//                } else {
//                    assert(addrDiff == -1L)
//                    Imgproc.cvtColorTwoPlane(yMat, uvMat2, rgba, Imgproc.COLOR_YUV2RGBA_NV21)
//                }
//                rgba
//            } else { // Chroma channels are not interleaved
//                val yuvBytes = ByteArray(w * (h + h / 2))
//                val yPlane = planes[0].buffer
//                val uPlane = planes[1].buffer
//                val vPlane = planes[2].buffer
//                var yuvBytesOffset = 0
//                val yPlaneStep = planes[0].rowStride
//                if (yPlaneStep == w) {
//                    yPlane[yuvBytes, 0, w * h]
//                    yuvBytesOffset = w * h
//                } else {
//                    val padding = yPlaneStep - w
//                    for (i in 0 until h) {
//                        yPlane[yuvBytes, yuvBytesOffset, w]
//                        yuvBytesOffset += w
//                        if (i < h - 1) {
//                            yPlane.position(yPlane.position() + padding)
//                        }
//                    }
//                    assert(yuvBytesOffset == w * h)
//                }
//                val chromaRowStride = planes[1].rowStride
//                val chromaRowPadding = chromaRowStride - w / 2
//                if (chromaRowPadding == 0) {
//                    // When the row stride of the chroma channels equals their width, we can copy
//                    // the entire channels in one go
//                    uPlane[yuvBytes, yuvBytesOffset, w * h / 4]
//                    yuvBytesOffset += w * h / 4
//                    vPlane[yuvBytes, yuvBytesOffset, w * h / 4]
//                } else {
//                    // When not equal, we need to copy the channels row by row
//                    for (i in 0 until h / 2) {
//                        uPlane[yuvBytes, yuvBytesOffset, w / 2]
//                        yuvBytesOffset += w / 2
//                        if (i < h / 2 - 1) {
//                            uPlane.position(uPlane.position() + chromaRowPadding)
//                        }
//                    }
//                    for (i in 0 until h / 2) {
//                        vPlane[yuvBytes, yuvBytesOffset, w / 2]
//                        yuvBytesOffset += w / 2
//                        if (i < h / 2 - 1) {
//                            vPlane.position(vPlane.position() + chromaRowPadding)
//                        }
//                    }
//                }
//                val yuvMat = Mat(h + h / 2, w, CvType.CV_8UC1)
//                yuvMat.put(0, 0, yuvBytes)
//                Imgproc.cvtColor(yuvMat, rgba, Imgproc.COLOR_YUV2RGBA_I420, 4)
//                rgba
//            }
//        }
//    }
//
//    private fun getPictureUri(): Uri {
//        val tmpFile = File.createTempFile(
//            "${System.currentTimeMillis()}",
//            ORIG_PICTURE_SUFFIX,
//            getPicturesDir()
//        ).apply {
//            createNewFile()
//            deleteOnExit()
//        }
//        return FileProvider.getUriForFile(this, "${this.packageName}$PROVIDER", tmpFile)
//    }
//
//    private fun getPicturesDir(): File =
//        if (Environment.getExternalStorageState() == Environment.MEDIA_MOUNTED) {
//            this.getExternalFilesDir(Environment.DIRECTORY_PICTURES)
//                ?: File(this.filesDir.absolutePath + Environment.DIRECTORY_PICTURES).also {
//                    if (!it.exists()) {
//                        it.mkdir()
//                    }
//                }
//        } else {
//            val internalImageDir = File(this.filesDir.absolutePath + Environment.DIRECTORY_PICTURES)
//            if (!internalImageDir.exists()) {
//                internalImageDir.mkdir()
//            }
//            internalImageDir
//        }
//
//    companion object {
//        private const val FILENAME_FORMAT = "yyyy-MM-dd-HH-mm-ss-SSS"
//        private const val ORIG_PICTURE_SUFFIX = ".png"
//        private const val PROVIDER = ".provider"
//    }
//}*/
