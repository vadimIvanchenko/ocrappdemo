package com.example.ocrapp.viewmodels

import android.net.Uri
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class MainViewModel : ViewModel() {
    private val _originalUri = MutableLiveData<Uri>()
    val originalUri: LiveData<Uri> = _originalUri

    private val _croppedImageUri = MutableLiveData<Pair<Uri, Boolean>>()
    val croppedImageUri: LiveData<Pair<Uri, Boolean>> = _croppedImageUri

    fun onTakeImage(uri: Uri, isTemporaryImage: Boolean) {
        _croppedImageUri.value = Pair(uri, isTemporaryImage)
    }

    fun onGetOriginalUri(uri: Uri) {
        _originalUri.value = uri
    }
}