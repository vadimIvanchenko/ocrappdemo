package com.example.ocrapp.fragments

import android.content.ContentValues
import android.graphics.*
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.util.DisplayMetrics
import android.util.Log
import android.util.Rational
import android.view.*
import androidx.camera.core.*
import androidx.camera.lifecycle.ProcessCameraProvider
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import com.example.ocrapp.viewmodels.MainViewModel
import com.example.ocrapp.databinding.FragmentCameraBinding
import com.example.ocrapp.log
import com.google.common.util.concurrent.ListenableFuture
import org.opencv.imgproc.Imgproc
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors

class CameraFragment : Fragment() {
    private val viewModel: MainViewModel by activityViewModels()
    private lateinit var binding: FragmentCameraBinding
    private lateinit var cameraProviderFuture: ListenableFuture<ProcessCameraProvider>
    private lateinit var canvas: Canvas
    private lateinit var paint: Paint
//    private lateinit var holder: SurfaceHolder

    private var imageCapture: ImageCapture? = null
//    private var imageAnalyzer: ImageAnalysis? = null

    private var cameraHeight: Int = 0
    private var cameraWidth: Int = 0
    private var xOffset: Int = 0
    private var yOffset: Int = 0
    private var boxWidth: Int = 0
    private var boxHeight: Int = 0

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentCameraBinding.inflate(inflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        startCamera()

//        binding.overlay.setZOrderOnTop(true)
//        holder = binding.overlay.holder
//        holder.setFormat(PixelFormat.TRANSLUCENT)
//        holder.addCallback(this)

        binding.imageCaptureButton.setOnClickListener { takePhoto() }
    }

    private fun startCamera() {
        cameraProviderFuture = ProcessCameraProvider.getInstance(requireContext())

        cameraProviderFuture.addListener({
            val cameraProvider = cameraProviderFuture.get()
            bindPreview(cameraProvider)
        }, ContextCompat.getMainExecutor(requireContext()))
    }

    private fun bindPreview(cameraProvider: ProcessCameraProvider) {
        val preview = Preview.Builder()
            .build()
            .also { it.setSurfaceProvider(binding.viewFinder.surfaceProvider) }

        imageCapture = ImageCapture.Builder()
            .build()

        val cameraSelector = CameraSelector.DEFAULT_BACK_CAMERA


        val viewPort = ViewPort.Builder(Rational(350, 100), Surface.ROTATION_0).build()

        val useCaseGroup = UseCaseGroup.Builder()
            .addUseCase(preview)
            .addUseCase(imageCapture!!)
            .setViewPort(viewPort)
            .build()

        try {
            cameraProvider.unbindAll()

            cameraProvider.bindToLifecycle(
                this, cameraSelector, useCaseGroup
            )

            cameraProvider.bindToLifecycle(
                this, cameraSelector, preview, imageCapture
            )

        } catch (exc: Exception) {
            log("$exc")
        }
    }

    private fun takePhoto() {
        val imageCapture = imageCapture ?: return

        val name = SimpleDateFormat(FILENAME_FORMAT, Locale.US).format(System.currentTimeMillis())
        val contentValues = ContentValues().apply {
            put(MediaStore.MediaColumns.DISPLAY_NAME, name)
            put(MediaStore.MediaColumns.MIME_TYPE, "image/jpeg")
            if (Build.VERSION.SDK_INT > Build.VERSION_CODES.P) {
                put(MediaStore.Images.Media.RELATIVE_PATH, "Pictures/CameraX-Image")
            }
        }

        val outputOptions = ImageCapture.OutputFileOptions
            .Builder(
                requireActivity().contentResolver,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                contentValues
            )
            .build()

        imageCapture.takePicture(
            outputOptions,
            ContextCompat.getMainExecutor(requireContext()),
            object : ImageCapture.OnImageSavedCallback {
                override fun onError(exc: ImageCaptureException) {
                    Log.d("qwerty", "Photo capture failed: ${exc.message}", exc)
                }

                override fun onImageSaved(output: ImageCapture.OutputFileResults) {
                    output.savedUri?.let {
                        findNavController().navigateUp()
                        viewModel.onTakeImage(it, true)
                    }
                }
            }
        )

    }

//    private fun DrawFocusRect(color: Int) {
//        val displaymetrics = DisplayMetrics()
//        requireActivity().windowManager.defaultDisplay.getMetrics(displaymetrics)
//        val height = binding.viewFinder.height
//        val width = binding.viewFinder.width
//
//        //cameraHeight = height;
//        //cameraWidth = width;
//        val left: Int
//        val right: Int
//        val top: Int
//        val bottom: Int
//        var diameter: Int
//        diameter = width
//        if (height < width) {
//            diameter = height
//        }
//        val offset = (0.05 * diameter).toInt()
//        diameter -= offset
//        canvas = holder.lockCanvas()
//        canvas.drawColor(0, PorterDuff.Mode.CLEAR)
//        //border's properties
//        paint = Paint()
//        paint.style = Paint.Style.STROKE
//        paint.color = color
//        paint.strokeWidth = 5f
//
//        left = width / 2 - diameter / 3
//        top = height / 2 - diameter / 3
//        right = width / 2 + diameter / 3
//        bottom = height / 2 + diameter / 3
//
//        xOffset = left
//        yOffset = top
//        boxHeight = bottom - top
//        boxWidth = right - left
//        //Changing the value of x in diameter/x will change the size of the box ; inversely proportionate to x
//        canvas.drawRect(left.toFloat(), top.toFloat(), right.toFloat(), bottom.toFloat(), paint)
//        holder.unlockCanvasAndPost(canvas)
//    }

    companion object {
        private const val FILENAME_FORMAT = "yyyy-MM-dd-HH-mm-ss-SSS"
    }
}