package com.example.ocrapp.fragments

import android.Manifest
import android.annotation.SuppressLint
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.media.Image
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.app.ActivityCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import com.example.ocrapp.databinding.FragmentStartBinding
import com.example.ocrapp.log
import com.example.ocrapp.viewmodels.MainViewModel
import com.google.mlkit.vision.common.InputImage
import com.google.mlkit.vision.text.TextRecognition
import com.google.mlkit.vision.text.latin.TextRecognizerOptions
import org.opencv.android.Utils
import org.opencv.core.CvType
import org.opencv.core.Mat
import org.opencv.core.Size
import org.opencv.imgcodecs.Imgcodecs
import org.opencv.imgproc.Imgproc
import java.io.File

class StartFragment : Fragment() {
    private val viewModel: MainViewModel by activityViewModels()
    private lateinit var binding: FragmentStartBinding

    private val requestPermissionsResult = registerForActivityResult(
        ActivityResultContracts.RequestMultiplePermissions()
    ) { permissions ->
        if (permissions.entries.all { it.value }) {
            findNavController().navigate(StartFragmentDirections.actionStartFragmentToCameraFragment())
        } else {
            Toast.makeText(requireContext(), "Permissions are denied", Toast.LENGTH_LONG).show()
        }
    }

    private val getContent =
        registerForActivityResult(ActivityResultContracts.GetContent()) { uri ->
            uri?.let {
                viewModel.onGetOriginalUri(uri)
                findNavController().navigate(StartFragmentDirections.actionStartFragmentToCroppingFragment())
            }
        }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentStartBinding.inflate(inflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.cameraButton.setOnClickListener {
            if (ActivityCompat.checkSelfPermission(
                    requireContext(),
                    Manifest.permission.CAMERA
                ) == PackageManager.PERMISSION_GRANTED
            ) {
                findNavController().navigate(StartFragmentDirections.actionStartFragmentToCameraFragment())
            } else {
                requestPermissionsResult.launch(
                    arrayOf(
                        Manifest.permission.CAMERA,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.READ_EXTERNAL_STORAGE
                    )
                )
            }
        }
        binding.galleryButton.setOnClickListener { getContent.launch("image/*") }

        /** WORKING CODE */
//        binding.recognizeMlKitButton.setOnClickListener {
//            viewModel.croppedImageUri.value?.let {
//                recognizeProcess(it.first, it.second)
//            } ?: Toast.makeText(requireContext(), "Choose an image please", Toast.LENGTH_LONG)
//                .show()
//        }

//        binding.recognizeTextractButton.setOnClickListener {}

        /** BEGIN OF TESTING CODE */

        binding.recognizeMlKitOpencvButton.setOnClickListener {
            viewModel.croppedImageUri.value?.let {
                val img = imageProcessing(it.first)
                recognizeProcessCV(img, it.first, it.second)
            } ?: Toast.makeText(requireContext(), "Choose an image please", Toast.LENGTH_LONG)
                .show()
        }



        binding.recognizeMlKitButton.setOnClickListener {
            viewModel.croppedImageUri.value?.let {
                recognizeProcess(it.first, it.second)
            } ?: Toast.makeText(requireContext(), "Choose an image please", Toast.LENGTH_LONG)
                .show()
        }
        /** END OF TESTING CODE */


        viewModel.croppedImageUri.observe(viewLifecycleOwner) {
            binding.initialText.visibility = View.GONE
            binding.image.setImageURI(it.first)
            binding.image.visibility = View.VISIBLE
        }
    }

    private fun recognizeProcess(uri: Uri, isTemporaryPicture: Boolean) {
        val image = InputImage.fromFilePath(requireContext(), uri)
        val recognizer = TextRecognition.getClient(TextRecognizerOptions.DEFAULT_OPTIONS)
        val list: MutableList<String> = mutableListOf()

        recognizer.process(image)
            .addOnSuccessListener { visionText ->
                visionText.textBlocks.forEach {
                    it.lines.forEach { line ->
                        list.add(line.text)
                    }
                }

                var message = ""
                for (i in list) {
                    message += "$i\n"
                }
                val result = message.ifEmpty { "Picture has no text symbols" }
                binding.text.text = result
                binding.text.visibility = View.VISIBLE

                binding.initialText.visibility = View.GONE
                binding.image.setImageURI(uri)
                binding.image.visibility = View.VISIBLE

//              DELETE TEMPORARY IMAGE
                if (isTemporaryPicture) requireActivity().contentResolver.delete(uri, null, null)
            }
            .addOnFailureListener { e ->
                Log.d("qwerty", "$e")
            }
    }

    private fun recognizeProcessCV(img: Bitmap, uri: Uri, isTemporaryPicture: Boolean) {
        val image = InputImage.fromBitmap(img, 0)
        val recognizer = TextRecognition.getClient(TextRecognizerOptions.DEFAULT_OPTIONS)
        val list: MutableList<String> = mutableListOf()

        recognizer.process(image)
            .addOnSuccessListener { visionText ->
                visionText.textBlocks.forEach {
                    it.lines.forEach { line ->
                        list.add(line.text)
                    }
                }

                var message = ""
                for (i in list) {
                    message += "$i\n"
                }
                val result = message.ifEmpty { "Picture has no text symbols" }
                binding.text.text = result
                binding.text.visibility = View.VISIBLE

                binding.initialText.visibility = View.GONE
                binding.image.setImageBitmap(img)
                binding.image.visibility = View.VISIBLE

//              DELETE TEMPORARY IMAGE
                if (isTemporaryPicture) requireActivity().contentResolver.delete(uri, null, null)
            }
            .addOnFailureListener { e ->
                Log.d("qwerty", "$e")
            }
    }

    private fun imageProcessing(uri: Uri): Bitmap {
        val fact = BitmapFactory.Options()
        fact.inPreferredConfig = Bitmap.Config.ARGB_8888
        val bmp = MediaStore.Images.Media.getBitmap(requireActivity().contentResolver, uri)
        val mat = Mat(bmp.width, bmp.height, CvType.CV_8UC4)
        Utils.bitmapToMat(bmp, mat)

        Imgproc.cvtColor(mat, mat, Imgproc.COLOR_BGR2GRAY)

//        Imgproc.threshold(mat, mat, 127.0, 255.0, Imgproc.THRESH_BINARY)

        Imgproc.adaptiveThreshold(
            mat, mat, 255.0, Imgproc.ADAPTIVE_THRESH_GAUSSIAN_C,
            Imgproc.THRESH_BINARY, 501, 2.0
        )
//        Imgproc.adaptiveThreshold(
//            mat, mat, 255.0, Imgproc.ADAPTIVE_THRESH_MEAN_C,
//            Imgproc.THRESH_BINARY, 11, 2.0
//        )
//
        val bitmap = Bitmap.createBitmap(mat.width(), mat.rows(), Bitmap.Config.ARGB_8888)
            .also {
                Utils.matToBitmap(mat, it)
            }
        return bitmap
    }
}