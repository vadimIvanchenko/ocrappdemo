package com.example.ocrapp.fragments

import android.graphics.Rect
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import com.canhub.cropper.CropImageView
import com.example.ocrapp.databinding.FragmentCroppingBinding
import com.example.ocrapp.viewmodels.MainViewModel

class CroppingFragment : Fragment(), CropImageView.OnSetImageUriCompleteListener,
    CropImageView.OnCropImageCompleteListener {
    private val viewModel: MainViewModel by activityViewModels()
    private lateinit var binding: FragmentCroppingBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentCroppingBinding.inflate(inflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setOptions()

        with(binding) {
            cropImageView.setOnSetImageUriCompleteListener(this@CroppingFragment)
            cropImageView.setOnCropImageCompleteListener(this@CroppingFragment)
            cropButton.setOnClickListener { cropImageView.croppedImageAsync() }
            rotateButton.setOnClickListener { cropImageView.rotateImage(90) }

            viewModel.originalUri.observe(viewLifecycleOwner) { uri ->
                cropImageView.setImageUriAsync(uri)
            }
        }
    }

    override fun onCropImageComplete(view: CropImageView, result: CropImageView.CropResult) {
        if (result.error == null) {
            result.uriContent?.let { uri ->
                findNavController().navigateUp()
                viewModel.onTakeImage(uri, false)
            }
//            SampleResultScreen.start(this, imageBitmap, result.uriContent, result.sampleSize)
        } else {
            Toast.makeText(activity, "Crop failed: ${result.error?.message}", Toast.LENGTH_SHORT)
                .show()
        }
    }

    override fun onSetImageUriComplete(view: CropImageView, uri: Uri, error: Exception?) {
        if (error != null) {
            Toast.makeText(activity, "Image load failed: " + error.message, Toast.LENGTH_LONG)
                .show()
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        binding.cropImageView.setOnSetImageUriCompleteListener(null)
        binding.cropImageView.setOnCropImageCompleteListener(null)
    }

    private fun setOptions() {
        binding.cropImageView.cropRect = Rect(100, 300, 500, 1200)
    }
}