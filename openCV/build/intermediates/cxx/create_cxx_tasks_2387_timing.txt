# C/C++ build system timings
create_cxx_tasks
  create-initial-cxx-model
    [gap of 15ms]
    create-X86-model 24ms
    [gap of 23ms]
    create-variant-model 10ms
    [gap of 25ms]
  create-initial-cxx-model completed in 97ms
create_cxx_tasks completed in 99ms

