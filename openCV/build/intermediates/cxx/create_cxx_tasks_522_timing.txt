# C/C++ build system timings
create_cxx_tasks
  create-initial-cxx-model
    create-module-model 10ms
    [gap of 23ms]
    create-X86-model 23ms
    create-X86_64-model 23ms
    create-variant-model 11ms
    create-ARMEABI_V7A-model 25ms
    create-ARM64_V8A-model 17ms
    [gap of 15ms]
  create-initial-cxx-model completed in 167ms
  [gap of 18ms]
create_cxx_tasks completed in 185ms

# C/C++ build system timings
create_cxx_tasks
  create-initial-cxx-model 52ms
create_cxx_tasks completed in 54ms

