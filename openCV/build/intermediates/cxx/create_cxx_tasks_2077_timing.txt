# C/C++ build system timings
create_cxx_tasks
  create-initial-cxx-model
    create-module-model 16ms
    create-variant-model 38ms
    [gap of 23ms]
    create-X86_64-model 23ms
    [gap of 45ms]
  create-initial-cxx-model completed in 152ms
create_cxx_tasks completed in 155ms

# C/C++ build system timings
create_cxx_tasks
  create-initial-cxx-model 36ms
create_cxx_tasks completed in 38ms

# C/C++ build system timings
create_cxx_tasks
  create-initial-cxx-model 66ms
create_cxx_tasks completed in 69ms

